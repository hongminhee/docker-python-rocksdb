FROM python:3.7

RUN apt-get update -qq && \
    apt-get install -y -qq \
      build-essential \
      cmake \
      libsnappy-dev \
      zlib1g-dev \
      libbz2-dev \
      libgflags-dev && \
    rm -rf /var/lib/apt/lists/*

RUN curl -L https://github.com/facebook/rocksdb/archive/v5.15.10.tar.gz \
      | tar xvfz - && \
    cd rocksdb-*/ && \
    mkdir build && \
    cd build/ && \
    cmake .. && \
    make && \
    cd .. && \
    make install-shared INSTALL_PATH=/usr && \
    cd .. && \
    rm -rf rocksdb-*/
