docker-python-rocksdb
=====================

An unofficial Docker image, based on `python:3.7`, that RocksDB 5.x is
installed.  Distributed under Public Domain.
